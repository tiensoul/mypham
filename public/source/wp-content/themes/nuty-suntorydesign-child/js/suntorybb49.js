(function ($) {

/*
  $(".nuty-account").hover(function () {
    $(this).addClass("opened");
  }, function () {
    $(this).removeClass("opened");
  });

  var timer;

  $('.submenu-login').on('mouseover', function () {
    $(this).parent().addClass('opened');
  }).on('mouseout', function () {
    $(this).parent().removeClass('opened');
  })

  $(".mobile-nav .nologin").live("click", function (e) {
    e.preventDefault();
    $("body").addClass("login-form-popup");
    $("body").removeClass("act-mobile-menu");
    $(".website-wrapper ").removeClass("left-wrapp");
  });


  $(".close-login-form").live("click", function () {
    $("body").removeClass("login-form-popup");
  });

  $('.menu-item-has-children').live('hover', function () {
      $('.mask_menu').fadeIn(300);
   });

  $('.menu-item-has-children ').hover(
    function () {
      $('.mask_menu').fadeIn(300);
   });
   
  $('.mask_menu').hover(
    function () {
      $('.mask_menu').stop(true, true).fadeOut(300);
    }
  );

*/
  //var height_product_thumb = $('.product-grid-item .product-element-top').width();

  //$('.product-grid-item .product-element-top').css('height', height_product_thumb);

/*$(".single-product #secondary").stick_in_parent();*/

/*$('.single-product #secondary')
.on('sticky_kit:bottom', function(e) {
    $(this).parent().css('position', 'static');
})
.on('sticky_kit:unbottom', function(e) {
    $(this).parent().css('position', 'relative');
})*/

  $('.home-tab-header-wrap li').live('click', function () {
    var classid = '.nuty-tab-' + $(this).data("action");
    $(".site-content").hide();
    $('.home-tab-header-wrap li').removeClass("active");
    $(this).addClass("active");
    $(classid).show();
    $(".nuty-tab-product-cat .list-cat-wrapper").html("");
    if ($(this).data("action") == 'product-cat') {
      var data = {
        'action': 'tab_page_product_cat_home',
        'pageid': 151
      };
      jQuery.post(basel_settings.ajaxurl, data, function (response) {
        $(".nuty-tab-product-cat .list-cat-wrapper").html(response);
      });
    }
  });


  $('.home-slider').owlCarousel({
    items: 1,
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    navText: false,
    autoHeight: true,
  })


  $('.cn-item-wrapper').owlCarousel({
    items: 5,
    loop: true,
    margin: 10,
    nav: true,
    dots: false,
    navText: false,
    autoHeight: true,
  })


/*
  $('.cn-item-wrapper').slick({
     infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      vertical:true,
      verticalSwiping: true,
  });
*/

  if ($('body').width() > 480) {
    var $slider_height = ($('body').width() * 480) / 1600;
    $('.home-slider .slide-item').css('height', $slider_height);
    $('.home-slider > .owl-height').css('height', $slider_height);
  }

  var owl = $("#carousel-5519 .owl-carousel");

  $(window).bind("vc_js", function () {
    owl.trigger('refresh.owl.carousel');
  });

  var options = {
    rtl: $('body').hasClass('rtl'),
    items: 1,
    responsive: {
      979: {
        items: 1
      },
      768: {
        items: 1
      },
      479: {
        items: 1
      },
      0: {
        items: 1
      }
    },
    autoplay: true,
    autoplayTimeout: 5000,
    dots: true,
    nav: true,
    autoheight: true,
    slideBy: 'page',
    navText: false,
    loop: false,
    onRefreshed: function () {
      $(window).resize();
    }
  };

  owl.owlCarousel(options);
  var owl = $("#sun-blog .owl-carousel");

  $(window).bind("vc_js", function () {
    owl.trigger('refresh.owl.carousel');
  });

  var options = {
    rtl: $('body').hasClass('rtl'),
    items: 4,
    responsive: {
      979: {
        items: 4
      },
      768: {
        items: 3
      },
      479: {
        items: 2
      },
      0: {
        items: 1
      }
    },
    autoplay: false,
    autoplayTimeout: 5000,
    dots: true,
    nav: true,
    autoheight: true,
    slideBy: 'page',
    navText: false,
    loop: false,
    onRefreshed: function () {
      $(window).resize();
    }
  };

  owl.owlCarousel(options);

   var single_product_cart = $('.single-product-content .entry-summary .summary-inner  form.cart');
   if(single_product_cart.length){
     var single_product_cart_top = single_product_cart.offset().top;
     if ($(window).width() < 481) {
         $(window).scroll(function (event) {
            var single_product_cart_scroll = $(this).scrollTop();
        
       
            if (single_product_cart_scroll >= single_product_cart_top) {
              single_product_cart.addClass('suntory-single-product-cart-fixed');
            }else{
              single_product_cart.removeClass('suntory-single-product-cart-fixed');
            } 

         });

     }
   }

/*
  var el_top = $('.sticky-scroll-box');
  if(el_top.length){
    var top = el_top.offset().top
  }
  if ($(window).width() > 1170) {
    $(window).scroll(function (event) {
      var y = $(this).scrollTop();
      if (y >= top) {

        if (y >= $('.single-product-page').height() - 300) {
          $('.sticky-scroll-box').removeClass('fixed');
          var new_top = $('.single-product-page').height() - $('.sticky-scroll-box').height()-100;
          var transform = 'translate3d(0px,' + new_top + 'px,0px)';
          $('.sticky-scroll-box').css("transform", transform);

        } else {

          $('.sticky-scroll-box').addClass('fixed');
          var transform = '';
          $('.sticky-scroll-box').css("top", '110px');
          $('.sticky-scroll-box').css("transform", transform);
        }

      } else {
        var transform = '';
        $('.sticky-scroll-box').css("transform", transform);
        $('.sticky-scroll-box').css("top", '');
        $('.sticky-scroll-box').removeClass('fixed');

      }


      $('.sticky-scroll-box').width($('.sticky-scroll-box').parent().width());
    });
  }*/

  $('.suntory-sidebar-container').bind('resize', function(){
    alert( 'Height changed to' + $(this).height() );
})

  $(".update_address").submit(function(e){


      if ($('input[name=billing_first_name]').val() == "") {
        alert('Cập nhật họ');
        e.preventDefault();
      }
      if ($('input[name=billing_last_name]').val() == "") {
        alert('Cập nhật tên');
        e.preventDefault();
      }
      if ($('input[name=billing_phone]').val() == "") {
        alert('Cập nhật số điện thoại');
        e.preventDefault();
      }
      if ($('input[name=billing_email]').val() == "") {
        alert('Cập nhật email');
        e.preventDefault();
      }
      if ($('input[name=billing_address_1]').val() == "") {
        alert('Cập nhật địa chỉ');
        e.preventDefault();
      }
      if ($('select[name=billing_state]').val() == "") {
        alert('Cập nhật tỉnh thành');
        e.preventDefault();
      }
        
  });
 /* if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    $('.woocommerce-product-gallery .thumbnails').owlCarousel('destroy');
    $(".woocommerce-product-gallery .thumbnails").owlCarousel({
      loop: true,
      autoplay: true,
      items: 1,
      nav: true,
      autoplayHoverPause: true,
      animateOut: 'slideOutUp',
      animateIn: 'slideInUp'
    });
   
  }*/

  $('.feature-box .feature-box-inner').css("height", $('.feature-box').width()-100);

  $('.popup_swatch .swatches-item').live('click', function () {
      //var val=$(this).data("value");
      //alert('test');
      $('.popup_swatch .swatches-item').removeClass('active-swatch');
      $(this).addClass("active-swatch");
     
  });

  //$('.testclick').live('click', function () {
   // alert('test');
    // });

  $('.popup_swatch .apply_swatches_button_wrap button').live('click', function () {
      if(!$('.popup_swatch .swatches-item').hasClass('active-swatch') ){
        alert('Vui lòng chọn thuộc tính');
      }else{
        var val=$('.popup_swatch .active-swatch .basel-swatch' ).data("value");
        var bgcolor=$('.popup_swatch .active-swatch .basel-swatch' ).data("bgcolor");
        var termname=$('.popup_swatch .active-swatch .basel-swatch' ).data("termname");
        $('#pa_mau-son').val(val).change();
        $('.suntory-swatches-item-bg').css('background-color',bgcolor);
        $('.suntory-swatches-item-text').html(termname);
        $(".popup_swatch").hide();

      }
      var val=$(this).data("value");
  });
  
    
  jQuery('.quick-buy-button').click(function (e) {
    e.preventDefault();

    $.ajax({
      url: basel_settings.ajaxurl,
      data: {
        action: 'suntory_quick_add_to_cart',
        product_id: $(this).data('product-id'),
        product_type: $(this).data('product-type'),
        variation_id: $('.variations_form .variation_id').val()
      },
      method: 'post',
      success: function (data) {
        if (data == true) {
          window.location.href = 'https://nuty.vn/thanh-toan';
        } else {
          alert(data);
        }
        // if (data != 'ok') {
        //alert(data);
        //}

      },
    })
  });

if ($(".archive_description_wrappper").height() > 300) {
    $(".archive_description_wrappper").append('<p class="showmore mask"></p><button type="button" class="readmore-achrive">XEM THÊM</button>');
    $(".archive_description_wrappper").addClass("readmore-content");
  }

  $(".readmore-achrive").live("click", function (e) {
    $(".archive_description_wrappper").removeClass("readmore-content");
    $(this).html("ĐÓNG");
    $(this).addClass("close-achrive");
    $(this).removeClass("readmore-achrive");
    $('.showmore').removeClass("mask");
  });


  $(".close-achrive").live("click", function (e) {
    $(".archive_description_wrappper").addClass("readmore-content");
    $(this).html("XEM THÊM");
    $(this).addClass("readmore-achrive");
    $(this).removeClass("close-achrive");
    $('.showmore').addClass("mask");
  });


  if ($(".single-product-content-wrapper").height() > 300) {
    $(".single-product-content-wrapper").append('<p class="showmore mask"></p><button type="button" class="readmore-achrive">XEM THÊM</button>');
    $(".single-product-content-inner").addClass("readmore-content");
  }

  $(".readmore-achrive").live("click", function (e) {
    $(".single-product-content-inner").removeClass("readmore-content");
    $(this).html("ĐÓNG");
    $(this).addClass("close-achrive");
    $(this).removeClass("readmore-achrive");
    $('.showmore').removeClass("mask");
  });


  $(".close-achrive").live("click", function (e) {
    $(".single-product-content-inner").addClass("readmore-content");
    $(this).html("XEM THÊM");
    $(this).addClass("readmore-achrive");
    $(this).removeClass("close-achrive");
    $('.showmore').addClass("mask");
  });



    $(".menu-footer .f-dropdown-1").live("click", function (e) {
      e.preventDefault();
      var fid = $(this).data("id");
      $(".menu-footer .f-dropdown-1 i").removeClass("fa fa-angle-down");

     
         $(".menu-footer .f-dropdown-2 i").addClass("fa fa-angle-down");
         $(".fd2").hide();
          $(".fd2").removeClass("active");
      

      if ($(".fd1").hasClass("active")) {
        $(".menu-footer .f-dropdown-1 i").addClass("fa fa-angle-down");
        $(".fd1").hide();
        $(".fd1").removeClass("active");
      } else {
        $(".menu-footer .f-dropdown-1 i").addClass("fa fa-angle-up");
        $(".fd1").show();
        $(".fd1").addClass("active");
      }
  });


  $(".menu-footer .f-dropdown-2").live("click", function (e) {
      e.preventDefault();
   
      $(".menu-footer .f-dropdown-2 i").removeClass("fa fa-angle-down");

      
         $(".menu-footer .f-dropdown-1 i").addClass("fa fa-angle-down");
         $(".fd1").hide();
         $(".fd1").removeClass("active");

      if ($(".fd2").hasClass("active")) {
        $(".menu-footer .f-dropdown-2 i").addClass("fa fa-angle-down");
        $(".fd2").hide();
        $(".fd2").removeClass("active");
      } else {
        $(".menu-footer .f-dropdown-2 i").addClass("fa fa-angle-up");
        $(".fd2").show();
        $(".fd2").addClass("active");
      }
  });

   $(".menu-footer .f-dropdown-2").trigger('click');

   if ($("div").hasClass("suntory-single-product-countdown")) { /*FOUND*/
    $('.suntory-single-product-countdown').countdown($('.suntory-single-product-countdown').data('end-date'), function (event) {
      $(this).text(event.strftime('%D ngày %H:%M:%S'));
    });
  }

  
  $(".popup-quick-shop").hide();
  $(".quick-shop-close").live("click", function (e) {
    var productid = $(".popup-quick-shop .variations_form").data("product_id");
    $('div[data-id="' + productid + '"]').removeClass("quick-shop-shown");
    $('div[data-id="' + productid + '"]').removeClass("quick-shop-loaded");
    $(".popup-quick-shop .variations_form").remove();
    $(".popup-quick-shop").hide();
    //$(".popup-quick-shop-mask").remove();
    $("body").removeClass("popup-quick-shop-body");

  });
  $(".popup-quick-shop .mask").live("click", function (e) {
    var productid = $(".popup-quick-shop .variations_form").data("product_id");
    $('div[data-id="' + productid + '"]').removeClass("quick-shop-shown");
    $('div[data-id="' + productid + '"]').removeClass("quick-shop-loaded");
    $(".popup-quick-shop .variations_form").remove();
    $(".popup-quick-shop").hide();
    //$(".popup-quick-shop-mask").remove();
    $("body").removeClass("popup-quick-shop-body");

  });
  $(".basel-hover-alt .product_type_variable.add_to_cart_button").live("click", function (e) {
      e.preventDefault();
      var productid = $(this).data("product_id");
      // $('.mobile-nav').clone().appendTo(".popup-quick-shop");
      //alert(quick_form_html);
      // $(".popup-quick-shop").html(quick_form_html);
      $.ajax({
        url: basel_settings.ajaxurl,
        data: {
          action: 'suntory_quick_shop',
          id: productid
        },
        method: 'get',
        success: function (data) {
          var scrollTop = $(window).scrollTop();
          if ($(this).length) {
            var elementOffset = $('a[data-product_id="' + productid + '"]').offset().top;
          }
          var distance = (elementOffset - scrollTop);
          // insert variations form
          $(".popup-quick-shop .container").append(data);
          $(".popup-quick-shop").show();
          $("body").addClass("popup-quick-shop-body");

          // $(".popup-quick-shop").css({
          //"top": elementOffset
          //});
          // $("body").append('<div class="popup-quick-shop-mask"></div>');

        },

      });
  });

    $(".suntory-show-sidebar-btn").live("click", function () {
      if (!$('body').hasClass('offcanvas-sidebar-mobile')) {
        $('body').addClass('offcanvas-sidebar-mobile');
        $(".suntory-sidebar-container").prepend('<div class="woodmart-close-sidebar-btn"><i class="fa fa-close"></i> Đóng</div>');
        $(".navi-fixed ").hide();
      }
       
    });

     $(".woodmart-close-sidebar-btn").live("click", function () {
      if ($('body').hasClass('offcanvas-sidebar-mobile')) {
        $('body').removeClass('offcanvas-sidebar-mobile');
        $(this).remove();
        $(".navi-fixed ").show();
      }
       
    });


$('.wo-readmore').live('click', function () {
    $('.content-readmore').removeClass("close");
    $('.wo-readmore').text('Rút gọn');
    $('.wo-readmore').addClass('wo-close');
    $('.wo-close').removeClass('wo-readmore');
    $('.wo-close').removeClass('btn-readmore');
    $('.showmore').removeClass('mask');

  });
  $('.wo-close').live('click', function () {
    $('.content-readmore').addClass("close");
    $('.wo-close').text('Đọc thêm');
    $('.wo-close').addClass('wo-readmore');
    $('.wo-readmore').removeClass('wo-close');
    $('.wo-readmore').addClass('btn-readmore');
    $('.showmore').addClass('mask');

  });

  if ($('#suntory_tab_description').height() > 400) {
     $('#suntory_tab_description .woocommerce-Tabs-panel--description').addClass("content-readmore close");
     $('#suntory_tab_description').append('<p class="showmore mask showmore"></p><button class="btn-readmore wo-readmore" style="margin: 20px auto;display: block;"> <span></span>Đọc thêm</button>');
  }

  var desc_single_product=$('.woocommerce-Tabs-panel--description > *').clone();
  $('.single-product-content-inner').append(desc_single_product);

  var meta_single_product=$('.woocommerce-Tabs-panel--additional_information > *').clone();
  $('.single-product-meta-wrapper').append(meta_single_product);

  $('.entry-summary .product_meta').hide();

  var product_related_price=$('.product-image-summary .product-related-price-wrapper > *').clone();
  $('.mobile-tab .product-related-price-wrapper').append(product_related_price);

  var review_single_product=$('.woocommerce-Tabs-panel--reviews > *').clone();
  $('.customer-content').append(review_single_product);


   var dataArr = {};
    // Load the locations once, on page-load.
    $(function() { 
        $.getJSON( "https://nuty.vn/suntory-app/listAll.json").done(function(data) {
            dataArr = data;
            //console.log(dataArr);
        }).fail(function(data) {
            console.log('no results found');
            //window.dataArr = testData; // remove this line in non-demo mode
        });
    });
    // Respond to any input change, and show first few matches
    function formatCurrency(number){
        var n = number.split('').reverse().join("");
        var n2 = n.replace(/\d\d\d(?!$)/g, "$&,");    
        return  n2.split('').reverse().join('') ;
    }
    function ChangeToSlug(title)
    {
        var  slug;
     
        //Lấy text từ thẻ input title 
        //title = document.getElementById("title").value;
     
        //Đổi chữ hoa thành chữ thường
        slug = title.toLowerCase();
     
        //Đổi ký tự có dấu thành không dấu
        //slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
        //slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
        //slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
        //slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
        //slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
        //slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
        //slug = slug.replace(/đ/gi, 'd');
        //Xóa các ký tự đặt biệt
        slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
        //Đổi khoảng trắng thành ký tự gạch ngang
        slug = slug.replace(/ /gi, " + ");
        //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
        //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
        slug = slug.replace(/\-\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-\-/gi, '-');
        slug = slug.replace(/\-\-\-/gi, '-');
        slug = slug.replace(/\-\-/gi, '-');
        //Xóa các ký tự gạch ngang ở đầu và cuối
        slug = '@' + slug + '@';
        slug = slug.replace(/\@\-|\-\@|\@/gi, '');
        //In slug ra textbox có id “slug”
       return slug;
    }
    $(".suntory-opt-search").live('keypress keyup change input', function() { 

        var count = 0;
        var i ='';
        var x='';
        var x = Array.from(x);
        var arrival = $(this).val().toLowerCase();
        console.log(arrival);
        var rid=$(this).data('rid');
        var searchdata=Array.from(dataArr);

        var product_list= searchdata.filter(function(place) {
                // look for the entry with a matching `code` value
                return (place.title.toLowerCase().indexOf(arrival) !== -1)  && count++ < 10;
        }).map(function(place) {
            // get titles of matches
            var price = formatCurrency(place.price);

            $.each(place.cat_list, function(i, e) {
                  x.push(e);
            });
        
            
            return '<div class="autocomplete-suggestion" data-index="1"> '+
            '<div class="suggestion-thumb"><a href="'+decodeURIComponent(place.link)+'"><img width="300" height="300" src="'+decodeURIComponent(place.thumb)+'"></a></div>'+
            '<div><h4 class="suggestion-title result-title"><a href="'+decodeURIComponent(place.link)+'">'+place.title+'</a></h4>' +
            '<div class="suggestion-price price"><span class="woocommerce-Price-amount amount">'+price+'<span class="woocommerce-Price-currencySymbol">₫</span></span></div></div></div>';
        }).join('\n');

        var product_cat_list='';
        var temp='';
        var temp = Array.from(temp);
        x.forEach(function(item, index) {
            if (jQuery.inArray( item.term_id, temp ) === -1 && temp.length < 4) {
                temp.push(item.term_id);
                product_cat_list = product_cat_list + '<div class="cat-item-search cat-'+item.term_id+'">'+arrival+' trong <a class="" href="http://nuty.vn/?s='+ChangeToSlug(arrival)+'&post_type=product&product_cat='+item.slug+'">'+item.name+'</a></div>';
    
            } 
                //console.log(val);
        });
       
        //console.log(product_cat_list);

        var html_search='<div class="search-wrapper">'+product_cat_list+'</div>' + product_list;

        $('.woodmart-search-form .res-matches-'+rid).html(!arrival.length ? '' : html_search);
    });

    $("body").live("click" , function(e){
         $('.woodmart-search-form .matches').html( ' ');

    });

})(jQuery);