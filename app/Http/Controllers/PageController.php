<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Product;
use App\Slide;
use App\TypeProduct;
use App\User;
use Hash;
use Auth;
use App\Cart;
use App\Customer;
use App\Bill;
use App\BillDetail;


class PageController extends Controller
{
    public function getIndex()
    {
        $slide = Slide::all()->take(8);
        //$productType = ProductType::all()->take(5);

        // lay 6 san pham dang khuyen mai
        $product_sale = Product::inRandomOrder()->where('promotion_price', '<>', 0)->take(6)->get();

        //lay 6 san pham trang diem
        $product_makeup = Product::inRandomOrder()->where('id_type', 1)->take(6)->get();

        //lay 6 san pham cham soc da
        $product_skincare = Product::inRandomOrder()->where('id_type', 2)->take(6)->get();

        //lay 6 san pham nuoc hoa
        $product_perfume = Product::inRandomOrder()->where('id_type', 5)->take(6)->get();

        //lay 6 san pham phu kien
        $product_accessories = Product::inRandomOrder()->where('id_type', 6)->take(6)->get();
        //dd($product_sale);

    	return view('pages.trangchu', compact('slide', 'product_sale', 'product_makeup', 'product_skincare', 'product_perfume', 'product_accessories'));
    }

    public function getChitiet($id)
    {
        $product = Product::find($id);
        //dd($product);
        $product_type = TypeProduct::join('product', 'type_product.id', '=', 'product.id_type')->get();
        //$product_type = $product->type_product;
        $product_random = Product::all()->random(4);
    	return view('pages.chitiet', compact('product', 'product_type' ,'product_random'));
    }

    public function getLoaiSP($type)
    {

        //phan trang 12 san pham tren mot trang
        $product_type = Product::where('id_type', $type)->paginate(12);
        //dd($product_type);
        $product_type_name = TypeProduct::where('id', $type)->first();
        //dd($product_type_name);

        $loaisp = TypeProduct::all();

    	return view('pages.loaisanpham', compact('product_type', 'product_type_name', 'loaisp'));
    }

    public function getTimKiem(Request $request)
    {
        $keyshow = $request->keyword;
        $loaisp = TypeProduct::all();
        $search_result = Product::where('name', 'like', '%'. $request->keyword . '%')->orWhere('unit_price', $request->keyword)->paginate(6);
        //dd($search_result);
        return view('pages.timkiem', compact('search_result', 'loaisp', 'keyshow'));
    }

    public function getTaiKhoan()
    {
        return view('pages.taikhoan');
    }

    public function getDangKy()
    {
        return view('pages.taikhoan');
    }

    public function postDangKy(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60|unique:users,email',
            'hoten' => 'required|max:50',
            'diachi' => 'required|max:255',
            'sodienthoai' => 'required|max:11',
            'gioitinh' => 'required',
            'password' => 'required|max:20',
            'repassword' => 'required|max:20|same:password'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống. ',
            'email.max' => 'Email không vượt quá 60 kí tự. ',
            'email.unique' => 'Email đã có người sử dụng vui lòng kiểm tra lại. ',
            'hoten.required' => 'Họ tên không được để trống. ',
            'hoten.max' => 'Họ tên không vượt quá 60 kí tự. ',
            'diachi.required' => 'Địa chỉ không được để trống. ',
            'diachi.max' => 'Địa chỉ không vượt quá 60 kí tự. ',
            'sodienthoai.required' => 'Số điện thoại không được để trống. ',
            'sodienthoai.max' => 'Số điện thoại không được vượt quá 11 kí tự. ',
            'gioitinh.required' => 'Giới tính không được để trống. ',
            'password.required' => 'Mật khẩu không được để trống. ',
            'password.max' => 'Mật khẩu không vượt quá 20 kí tự. ',
            'repassword.required' => 'Trường nhập lại mật khẩu không được để trống. ',
            'repassword.max' => 'Trường nhập lại mật khẩu không vượt quá 20 kí tự. ',
            'repassword.same' => 'Mật khẩu không giống nhau. '
        ]);

        $user = new User();
        $user->name = $request->hoten;
        $user->email = $request->email;
        $user->gender = $request->gioitinh;
        $user->password = Hash::make($request->password);
        $user->phone = $request->sodienthoai;
        $user->address = $request->diachi;
        $user->save();

        // $customer = new Customer();
        // $customer->name = $request->hoten;
        // $customer->email = $request->email;
        // $customer->gender = $request->gioitinh;
        // $customer->phone_number = $request->sodienthoai;
        // $customer->address = $request->diachi;
        // $customer->save();

        return redirect()->back()->with(['flag' => 'success', 'thongbao' => 'Bạn đã đăng ký tài khoản thành công bây giờ bạn có thể đăng nhập!']);
    }

    public function getDangNhap(Request $request)
    {
        return view('page.dangnhap');
    }

    public function postDangNhap(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|max:60',
            'password' => 'required|max:20'
        ],
        [
            'email.required' => 'Địa chỉ email không được để trống',
            'email.max' => 'Địa chỉ email tối đa 60 kí tự',
            'password.required' => 'Mật khẩu không được để trống',
            'password.max' => 'Mật khẩu tối đa 20 kí tự'
        ]);

        $info = array('email' => $request->email, 'password' => $request->password);
        if(Auth::attempt($info)) {
            return redirect('/')->with(['flag' => 'success', 'message' => 'Đăng nhập thành công!']);
        } else {
            return redirect()->back()->with(['flag' => 'danger', 'message' => 'Đăng nhập không thành công vui lòng kiểm tra lại!']);
        }
    }

    public function getDangXuat()
    {
        Auth::logout();
        return view('pages.taikhoan');
    }

    //Cart
    public function getThemSanPham(Request $request, $id)
    {
        $product = Product::find($id);
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        //dd(Session::has('cart'));
        $cart = new Cart($oldcart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);
        //dd($request->session()->get('cart'));

    	return redirect()->back();
    }

    public function getMuaNgay(Request $request, $id)
    {
        $product = Product::find($id);
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        //dd(Session::has('cart'));
        $cart = new Cart($oldcart);
        $cart->add($product, $product->id);

        $request->session()->put('cart', $cart);
        //dd($request->session()->get('cart'));

    	return redirect()->route('thanhtoan');
    }


    public function getXoaMotSanPham(Request $request, $id)
    {
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldcart);
        $cart->reduceByOne($id);

        Session::put('cart', $cart);
        // dd($request->session()->get('cart'));

    	return redirect()->back();
    }

    public function getXoaTatCaSP($id)
    {
        $oldcart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldcart);
        $cart->removeItem($id);

        Session::put('cart', $cart);

        return redirect()->back();
    }

    public function getGioHang(Request $request)
    {
        if(!Session::has('cart')) {
            return view('pages.thanhtoan', ['products' => null]);
        }
        $oldcart = Session::get('cart');
        $cart = new Cart($oldcart);
        //dd($cart->items, $cart->totalPrice,  $cart->totalPromotionPrice);
        return view('pages.giohang', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'totalPromotionPrice' => $cart->totalPromotionPrice]);
    }


    public function getThanhtoan(Request $request)
    {
        if(!Session::has('cart')) {
            return view('pages.thanhtoan', ['products' => null]);
        }
        $oldcart = Session::get('cart');
        $cart = new Cart($oldcart);
        //dd($cart->items, $cart->totalPrice,  $cart->totalPromotionPrice);
        return view('pages.thanhtoan', ['products' => $cart->items, 'totalPrice' => $cart->totalPrice, 'totalPromotionPrice' => $cart->totalPromotionPrice]);
    }

    public function postThanhToan(Request $request)
    {
        //dd($request);
        $this->validate($request, [
            'name' => 'required|max:255',
            'gender' => 'required|in:1,0',
            'email' => 'required|email',
            'address' => 'required|max:255',
            'phone' => 'max:13|required|min:10|digits_between:10,13'
        ],
        [
            'name.required' => 'Tên khách hàng không được để trống.',
            'name.max' => 'Tên khách hàng không vượt quá 255 ký tự.',
            'gender.required' => 'Giới tính không được để trống.',
            'email.required' => 'Địa chỉ email không được để trống.',
            'email.email' => 'Địa chỉ email không đúng định dạng.',
            'address.required' => 'Địa chỉ nhận hàng không được để trống.',
            'address.max' => 'Địa chỉ tối đa 255 ký tự',
            'phone.required' => 'Số điện thoại không được để trống.',
            'phone.digits_between' => 'Số điện thoại không đúng định dạng.',
            'phone.max' => 'Số điện thoại không quá 13 ký tự.',
            'phone.min' => 'Số điện thoại tối thiểu 10 ký tự'
        ]);
        $cart = Session::get('cart');
        //dd($cart);
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->gender = $request->gender;
        $customer->email = $request->email;
        $customer->address = $request->address;
        $customer->phone_number = $request->phone;
        if($request->ghichu == '')
            $customer->note = "Không có ghi chú";
        else
            $customer->note = $request->ghichu;
        $customer->save();

        $bill = new Bill;
        $bill->id_customer = $customer->id;
        $bill->total = $request->price_payment;
        $bill->payment = $request->payment_method;
        if($request->ghichu == '')
            $bill->note = "Không có ghi chú";
        else
            $bill->note = $request->ghichu;
        $bill->save();

        foreach ($cart->items as $key => $value) {
            $billdetail = new BillDetail;
             $billdetail->id_bill = $bill->id;
             $billdetail->id_product = $key;
             $billdetail->quantity = $value['qty'];
             $billdetail->unit_price = $value['item']['unit_price'];
             $billdetail->promotion_price = $value['item']['promotion_price'] == 0 ? 0 : $value['item']['promotion_price'];
             $billdetail->save();
        }
 
        
        Session::forget('cart');
        return redirect()->back()->with('thongbao', 'Đặt hàng thành công. Nhân viên giao hàng sẽ liên hệ với bạn trong thời gian sớm nhất!');


    }









}
