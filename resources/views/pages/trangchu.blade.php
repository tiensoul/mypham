@extends('pages/master')
@section('content')
<!-- MAIN CONTENT AREA -->
<div class="container">
<div class="row">
        <div class="site-content nuty-tab-product-cat" role="main" style="display: none;">
            <div class="list-cat-wrapper">

            </div>
        </div>
        <div class="site-content nuty-tab-home" role="main">
            <!---------HOME SLIDER SECTION---------->
            <div class="owl-carousel owl-theme home-slider">

                @foreach($slide as $sl)
                <div class="slide-banner">
                    <a href="#" target="_blank">
                        <div class="slide-item"
                            style="background:url('/{{ $sl->link }}');background-size: cover;background-repeat: no-repeat;">
                            <img src="{{ $sl->link }}">

                        </div>
                    </a>
                </div>
                @endforeach




            </div>
            <!---------SALE PRDOUCT SECTION---------->
            <div class="suntory-sale-products">
                <div class="container">
                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-bottom: 30px;">
                            <h3 class="home-title2">Sản phẩm khuyến mãi</h3>
                        </div>

                        <!--MAC DINH SAN PHAM SALE--->
                        <!----->
                        <div class="basel-products-element">
                            <div class="basel-products-loader"></div>
                            <div class="products elements-grid row basel-products-holder  pagination-arrows grid-columns-6">

                                @foreach($product_sale as $ps)
                                <div class="product-grid-item basel-hover-alt product  col-xs-6 col-sm-4 col-md-2 first  post-107506 type-product status-publish has-post-thumbnail product_cat-xit-khoa-nen product_tag-byphasse product_tag-byphasse-fix-make-up-long-lasting product_tag-sale2004 product_tag-salesn product_tag-xit-khoa-lop-trang-diem product_tag-xit-khoa-nen first instock shipping-taxable purchasable product-type-simple"
                                    data-loop="1" data-id="107506">

                                    <div class="product-element-top">
                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($ps->promotion_price / $ps->unit_price) * 100), 0) }}%</span></div>
                                        <a href="{{ route('chitiet', $ps->id) }}">
                                            <img width="300" height="300"
                                        src="uploads/product/{{ $ps->image }}"
                                                class="lazy lazy-hidden jetpack-lazy-image" alt="" />
                                        </a>
                                        <div class="basel-buttons">
                                        </div>
                                    </div>
                                    <h3 class="product-title"><a
                                    href="{{ route('chitiet', $ps->id) }}">{{ $ps->name }}</a></h3>

                                    <div class="wrap-price">
                                        <div class="wrapp-swap">
                                            <div class="swap-elements">

                                                <span class="price">
                                                <span class="woocommerce-Price-amount amount"><span style="text-decoration: line-through; color: #a52a2a;">{{ number_format(($ps->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($ps->promotion_price), 0, '.', ',') }}đ</span></span>
                                                <div class="btn-add">
                                                    <a href="{{ route('cartAdd', $ps->id) }}"
                                                        class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                @endforeach
                                <div class="clearfix visible-xs-block"></div>
                                <div class="clearfix visible-sm-block"></div>
                                <div class="clearfix visible-md-block visible-lg-block"></div>
                            </div>
                            <div class="products-footer">
                            </div>
                        </div>
                        <div class="basel-products-element">
                            <div class="basel-products-loader"></div>
                            <div class="products elements-grid row basel-products-holder  pagination-arrows grid-columns-6"
                                data-paged="1" data-source="shortcode"
                                data-atts="{&quot;post_type&quot;:&quot;product&quot;,&quot;layout&quot;:&quot;grid&quot;,&quot;include&quot;:&quot;&quot;,&quot;custom_query&quot;:&quot;&quot;,&quot;taxonomies&quot;:&quot;11759&quot;,&quot;pagination&quot;:&quot;arrows&quot;,&quot;items_per_page&quot;:&quot;6&quot;,&quot;product_hover&quot;:&quot;alt&quot;,&quot;columns&quot;:&quot;6&quot;,&quot;sale_countdown&quot;:0,&quot;offset&quot;:&quot;&quot;,&quot;orderby&quot;:&quot;date&quot;,&quot;query_type&quot;:&quot;OR&quot;,&quot;order&quot;:&quot;DESC&quot;,&quot;meta_key&quot;:&quot;&quot;,&quot;exclude&quot;:&quot;&quot;,&quot;class&quot;:&quot;&quot;,&quot;ajax_page&quot;:&quot;&quot;,&quot;speed&quot;:&quot;5000&quot;,&quot;slides_per_view&quot;:&quot;1&quot;,&quot;wrap&quot;:&quot;&quot;,&quot;autoplay&quot;:&quot;no&quot;,&quot;hide_pagination_control&quot;:&quot;yes&quot;,&quot;hide_prev_next_buttons&quot;:&quot;&quot;,&quot;scroll_per_page&quot;:&quot;yes&quot;,&quot;carousel_js_inline&quot;:&quot;no&quot;,&quot;img_size&quot;:&quot;woocommerce_thumbnail&quot;,&quot;force_not_ajax&quot;:&quot;no&quot;,&quot;center_mode&quot;:&quot;no&quot;,&quot;products_masonry&quot;:&quot;&quot;,&quot;products_different_sizes&quot;:&quot;&quot;}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!---------RECENT PRDOUCT SECTION---------->
            <div class="suntory-recent-products">
                <div class="container">

                    <div class="row">
                        <div class="hometitle-wrapper">
                            <h3 class="home-title2">Trang điểm</h3>
                        </div>
                        <div id="carousel-265" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_makeup as $pm)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">
                                        <div class="product-grid-item basel-hover-alt product product-in-carousel post-140115 type-product status-publish has-post-thumbnail product_cat-trang-diem product_cat-eyes-makeup product_cat-phan-mat first instock shipping-taxable purchasable product-type-simple"
                                            data-loop="1" data-id="140115">

                                            <div class="product-element-top">
                                                @if($pm->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pm->promotion_price / $pm->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a
                                                    href="{{ route('chitiet', $pm->id) }}">
                                                    <img width="300" height="300" src="uploads/product/{{ $pm->image }}"
                                                        class="lazy lazy-hidden jetpack-lazy-image" alt="" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                            href="{{ route('chitiet', $pm->id) }}">{{ $pm->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                        <span class="price"><span class="woocommerce-Price-amount amount">
                                                            @if($pm->promotion_price != 0)
                                                            <span style="text-decoration: line-through; color: #a52a2a;">
                                                                {{ number_format(($pm->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($pm->promotion_price), 0, '.', ',') }}đ
                                                            @else
                                                                {{ number_format(($pm->unit_price), 0, '.', ',') }}đ
                                                            @endif
                                                            </span>
                                                        </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $pm->id) }}" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-265 -->

                    </div>

                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-top: 20px;">
                            <h3 class="home-title2">Chăm sóc da</h3>
                        </div>
                        <div id="carousel-531" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_skincare as $psk)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">

                                        <div
                                            class="product-grid-item basel-hover-alt product product-in-carousel post-140065 type-product status-publish has-post-thumbnail product_cat-tay-te-bao-chet first instock shipping-taxable purchasable product-type-simple"
                                            data-loop="1" data-id="140065">

                                            <div class="product-element-top">
                                                @if($psk->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pm->promotion_price / $pm->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a
                                                    href="{{ route('chitiet', $psk->id) }}">
                                                    <img width="300" height="300" src="uploads/product/{{ $psk->image }}" class="lazy lazy-hidden jetpack-lazy-image" alt="" data-lazy-type="image" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                                    href="{{ route('chitiet', $psk->id) }}">{{ $psk->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                        <span class="price"><span class="woocommerce-Price-amount amount">
                                                                @if($psk->promotion_price != 0)
                                                                <span style="text-decoration: line-through; color: #a52a2a;">
                                                                    {{ number_format(($psk->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($psk->promotion_price), 0, '.', ',') }}đ
                                                                @else
                                                                    {{ number_format(($psk->unit_price), 0, '.', ',') }}đ
                                                                @endif
                                                                </span>
                                                        </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $psk->id) }}" data-quantity="1"
                                                                class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>

                                    </div>
                                </div>
                                @endforeach

                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-531 -->

                    </div>

                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-top: 20px;">
                            <h3 class="home-title2">Nước hoa</h3>
                        </div>
                        <div id="carousel-783" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_perfume as $pp)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">

                                        <div
                                            class="product-grid-item basel-hover-alt product product-in-carousel post-140122 type-product status-publish has-post-thumbnail product_cat-cham-soc-da product_cat-kem-duong-da first instock shipping-taxable purchasable product-type-variable has-default-attributes"
                                            data-loop="1" data-id="140122">

                                            <div class="product-element-top">
                                                @if($pp->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pp->promotion_price / $pp->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a href="{{ route('chitiet', $pp->id) }}">
                                                    <img width="300" height="300" src="uploads/product/{{ $pp->image }}" alt="" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                                    href="{{ route('chitiet', $pp->id) }}">{{ $pp->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                        <span class="price"><span class="woocommerce-Price-amount amount">
                                                                @if($pp->promotion_price != 0)
                                                                <span style="text-decoration: line-through; color: #a52a2a;">
                                                                    {{ number_format(($pp->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($pp->promotion_price), 0, '.', ',') }}đ
                                                                @else
                                                                    {{ number_format(($pp->unit_price), 0, '.', ',') }}đ
                                                                @endif
                                                                </span>
                                                        </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $pp->id) }}" class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-783 -->

                    </div>

                    <div class="row">
                        <div class="hometitle-wrapper" style="margin-top: 20px;">
                            <h3 class="home-title2">Phụ kiện</h3>
                        </div>
                        <div id="carousel-110" class="vc_carousel_container " data-owl-carousel
                            data-hide_pagination_control="yes" data-desktop="6" data-desktop_small="4" data-tablet="4"
                            data-mobile="2">
                            <div class="owl-carousel product-items ">

                                @foreach($product_accessories as $pa)
                                <div class="product-item owl-carousel-item">
                                    <div class="owl-carousel-item-inner">

                                        <div
                                            class="product-grid-item basel-hover-alt product product-in-carousel post-139236 type-product status-publish has-post-thumbnail product_cat-nuoc-hoa product_cat-nuoc-hoa-nu first instock shipping-taxable purchasable product-type-simple"
                                            data-loop="1" data-id="139236">

                                            <div class="product-element-top">
                                                @if($pa->promotion_price != 0)
                                                    <div class="product-labels labels-rectangular"><span class="onsale product-label">-{{ number_format((100 - ($pa->promotion_price / $pa->unit_price) * 100), 0) }}%</span></div>
                                                @endif
                                                <a href="{{ route('chitiet', $pa->id) }}">
                                                    <img width="300" height="300"
                                                        src="uploads/product/{{ $pa->image }}"
                                                        class="lazy lazy-hidden jetpack-lazy-image" alt="" />
                                                </a>
                                                <div class="basel-buttons">
                                                </div>
                                            </div>
                                            <h3 class="product-title"><a
                                                    href="{{ route('chitiet', $pa->id) }}">{{ $pa->name }}</a></h3>

                                            <div class="wrap-price">
                                                <div class="wrapp-swap">
                                                    <div class="swap-elements">

                                                            <span class="price"><span class="woocommerce-Price-amount amount">
                                                                    @if($pa->promotion_price != 0)
                                                                    <span style="text-decoration: line-through; color: #a52a2a;">
                                                                        {{ number_format(($pa->unit_price), 0, '.', ',') }}đ</span> - {{ number_format(($pa->promotion_price), 0, '.', ',') }}đ
                                                                    @else
                                                                        {{ number_format(($pa->unit_price), 0, '.', ',') }}đ
                                                                    @endif
                                                                    </span>
                                                            </span>
                                                        <div class="btn-add">
                                                            <a href="{{ route('cartAdd', $pa->id) }}"
                                                                class="button product_type_simple add_to_cart_button ajax_add_to_cart">Thêm vào giỏ</a> </div>
                                                    </div>
                                                </div>

                                            </div>



                                        </div>

                                    </div>
                                </div>
                                @endforeach
                            </div> <!-- end product-items -->
                        </div> <!-- end #carousel-110 -->

                    </div>

                </div>
            </div>
        </div><!-- .site-content -->

        <style type="text/css">
            @media (min-width: 1200px) {
                .chinhanh-item {
                    width: 20%;
                    padding: 0 5px 0 5px;
                }
            }
        </style>


</div> <!-- end row -->
</div> <!-- end container -->
@endsection('content')