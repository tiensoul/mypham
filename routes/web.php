<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'trangchu',
    'uses' => 'PageController@getIndex'
]);

Route::get('chi-tiet/{id}', [
	'as' => 'chitiet',
	'uses' => 'pageController@getChitiet'
]);

// Route::post('tim-kiem', [
// 	'as' => 'timkiem',
// 	'uses' => 'pageController@postTimKiem'
// ]);

Route::get('tim-kiem', [
	'as' => 'timkiem',
	'uses' => 'pageController@getTimKiem'
]);

Route::get('loai-sp/{type}', [
	'as' => 'loaisp',
	'uses' => 'pageController@getLoaiSP'
]);


Route::get('thanh-toan', function () {
    return view('pages/thanhtoan');
});

Route::get('gio-hang', [
	'as' => 'giohang',
	'uses' => 'pageController@getGioHang'
]);

Route::get('gio-hang', [
	'as' => 'giohang',
	'uses' => 'pageController@getThanhtoan'
]);

Route::post('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@postThanhToan'
]);

Route::get('cartRemoveOne/{id}', [
	'as' => 'cartRemoveOne',
	'uses' => 'pageController@getXoaMotSanPham'
]);


Route::get('tai-khoan', [
	'as' => 'taikhoan',
	'uses' => 'pageController@getDangKy'
]);

Route::post('tai-khoan', [
	'as' => 'taikhoan',
	'uses' => 'pageController@postDangKy'
]);

Route::get('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'PageController@getDangNhap'
]);

Route::post('dang-nhap', [
	'as' => 'dangnhap',
	'uses' => 'PageController@postDangNhap'
]);

Route::get('dang-xuat', [
	'as' => 'dangxuat',
	'uses' => 'PageController@getDangXuat'
]);


Route::get('gioi-thieu', function () {
    return view('pages/gioithieu');
});


Route::get('cach-mua-hang', function () {
    return view('pages/cachmuahang');
});


//cart
Route::get('cartAdd/{id}', [
	'as' => 'cartAdd',
	'uses' => 'PageController@getThemSanPham'
]);

Route::get('buyNow/{id}', [
	'as' => 'buyNow',
	'uses' => 'PageController@getMuaNgay'
]);

Route::get('gio-hang', [
	'as' => 'giohang',
	'uses' => 'pageController@getGioHang'
]);

Route::get('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@getThanhtoan'
]);

Route::post('thanh-toan', [
	'as' => 'thanhtoan',
	'uses' => 'pageController@postThanhtoan'
]);

Route::get('cartRemoveAll/{id}', [
	'as' => 'cartRemoveAll',
	'uses' => 'pageController@getXoaTatCaSP'
]);

