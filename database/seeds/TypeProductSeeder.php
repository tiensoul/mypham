<?php

use Illuminate\Database\Seeder;

class TypeProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_product')->insert([
            ['name' => 'Trang điểm', 'description' => 'Đồ trang điểm'],
            ['name' => 'Chăm sóc da', 'description' => 'Đồ trang điểm'],
            ['name' => 'Bodycare', 'description' => 'Đồ trang điểm'],
            ['name' => 'Chăm sóc tóc', 'description' => 'Đồ trang điểm'],
            ['name' => 'Nước hoa', 'description' => 'Đồ trang điểm'],
            ['name' => 'Phụ kiện', 'description' => 'Đồ trang điểm'],
            ['name' => 'Chăm sóc mặt', 'description' => 'Đồ trang điểm'],
            ['name' => 'Đồ tẩy trang', 'description' => 'Đồ trang điểm']
        ]);
    }
}
